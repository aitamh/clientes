package com.jlyh.cliente.service;

import com.jlyh.cliente.entity.Cliente;

public interface IClienteService {

    Cliente addCliente(Cliente cliente);

    Cliente updateCliente(Long idcliente);

    void deleteCliente(Long idcliente);

}
