package com.jlyh.cliente.controller;

import com.jlyh.cliente.entity.Cliente;
import com.jlyh.cliente.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClienteController {

    @Autowired
    private IClienteService clienteService;

    @PostMapping("guardar")
    private ResponseEntity<?> guardarCliente(@RequestBody Cliente cliente){

        this.clienteService.addCliente(cliente);

        return null;
    }

    @PostMapping("delete")
    private ResponseEntity<?> deleteCliente(@RequestParam Long idcliente){

        this.clienteService.deleteCliente(idcliente);

        return null;
    }

}
