package com.jlyh.cliente.service;

import com.jlyh.cliente.dao.IClienteDao;
import com.jlyh.cliente.entity.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl implements IClienteService{

    @Autowired
    private IClienteDao clienteDao;

    @Override
    public Cliente addCliente(Cliente cliente) {
        return this.clienteDao.save(cliente);
    }

    @Override
    public Cliente updateCliente(Long idcliente) {
        return null;
    }

    @Override
    public void deleteCliente(Long idcliente) {
        this.clienteDao.deleteById(idcliente);
    }
}
