package com.jlyh.cliente.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cliente")
@Getter
@Setter
@AllArgsConstructor
public class Cliente {

    private Long id;

    private String nombres;

    private String apellidos;

    private String dni;

    private String celular;

    private String correo;
}
